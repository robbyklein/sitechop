Rails.application.routes.draw do
  # Root Path
  match '*any' => 'application#options', :via => [:options]
  root 'posts#index'
  get 'tags/:tag', to: 'posts#index', as: :tag

  # Sessions
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  # Password Resets 
  get 'password_resets/new'
  get 'password_resets/edit'

  # Pages
  get 'admin' => 'pages#admin'
  get 'about' => 'pages#about'
  get 'signup'  => 'users#new'
  get '/codepen', to: 'posts#codepen', defaults: { format: 'json' }
  get 'feed' => 'posts#index', defaults: { format: 'atom' }
 
  # Model Pages
  resources :posts, except: [:show, :codepen] do 
    resources :comments, only: [ :create ]
  end

  resources :categories, except: [:show]
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  
  # Category Show
  get '/:permalink', to: 'categories#show'
  
  # Post Show
  get '/:category/:permalink', to: 'posts#show', as: :post_show



end
