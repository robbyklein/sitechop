class AddAttachmentSourceCodeToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :source
    end
  end

  def self.down
    remove_attachment :posts, :source
  end
end
