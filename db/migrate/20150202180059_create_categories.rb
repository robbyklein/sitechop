class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.text :description
      t.string :permalink
      t.string :meta

      t.timestamps null: false
    end
  end
end
