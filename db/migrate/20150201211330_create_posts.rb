class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :markdown
      t.text :html
      t.text :excerpt
      t.string :meta
      t.string :permalink
      t.string :codepen
      t.references :category, index: true

      t.timestamps null: false
    end
  end
end
