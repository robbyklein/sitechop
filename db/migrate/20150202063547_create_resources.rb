class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :title
      t.text :description
      t.string :permalink
      t.string :meta

      t.timestamps null: false
    end
  end
end
