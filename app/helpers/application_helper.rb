module ApplicationHelper
  def avatar_url(user)

      default_url = "#{root_url}images/guest.png"
      gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
      "http://gravatar.com/avatar/#{gravatar_id}.png?s=200&d=#{CGI.escape(default_url)}"
  end

  def the_title
    if @title.present?
      return "#{@title} | Sitechop"
    else
      return 'Sitechop'
    end
  end
end
