class ApplicationMailer < ActionMailer::Base
  default from: "noreply@sitechop.com"
  layout 'mailer'
end
