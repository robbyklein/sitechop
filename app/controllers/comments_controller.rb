class CommentsController < ApplicationController
  before_action :find_scribe

  def create
    if logged_in?
      @comment = @post.comments.build(comment_params)
      @comment.user_id = current_user.id

      if @comment.save
        redirect_to '/'
      end
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:comment, :user_id, :post_id)
  end

  def find_scribe
    @post = Post.find_by!(permalink: params[:post_id])
  end
end
