class CategoriesController < ApplicationController
  def new
    if logged_in? && current_user.admin?
      @category = Category.new
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def create
    if logged_in? && current_user.admin?

      @category = Category.new(category_params)

      if @category.save
        redirect_to @category
      else
        redirect_to root
      end

    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def edit
    if logged_in? && current_user.admin?
      @category = Category.find_by!(permalink: params[:id])
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def update
    if logged_in? && current_user.admin?
      @category = Category.find_by!(permalink: params[:id])

      if @category.update(category_params)
        redirect_to @category.url
      else
        redirect_to root
      end

    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end      
  end

  def index
    @categories = Category.all
  end

  def show
    @category = Category.find_by!(permalink: params[:permalink])
    @posts = @category.posts.order("created_at DESC").page(params[:page]).per(8)
  end

  def destroy
    if logged_in? && current_user.admin?
      @category = Category.find_by!(permalink: params[:permalink])
      @category.destroy
      redirect_to admin_path
    else
      redirect_to "/"
      flash[:error] = "Please Don't :("
    end
  end

  private
    def category_params
      params.require(:category).permit(:title, :description, :image, :permalink)
    end
end
