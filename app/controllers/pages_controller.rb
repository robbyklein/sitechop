class PagesController < ApplicationController
  def about
    @posts = Post.all
  end

  def admin
    if logged_in? && current_user.admin?
      @posts = Post.all.order("created_at DESC")
      @resources = Resource.all.order("created_at DESC")
      @categories = Category.all.order("created_at DESC")
      @users = User.all.order("created_at DESC")
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end
end
