class PostsController < ApplicationController
  def new
    if logged_in? && current_user.admin?
      @post = Post.new
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def codepen
    @posts = Post.order("RAND()").last(2)
    respond_to :json
  end

  def create
    if logged_in? && current_user.admin?

      @post = Post.new(post_params)

      if @post.save
        redirect_to @post.url
      else
        redirect_to root
      end

    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def edit
    if logged_in? && current_user.admin?
      @post = Post.find_by!(permalink: params[:id])
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def update
    if logged_in? && current_user.admin?

      @post = Post.find_by!(permalink: params[:id])

      if @post.update(post_params)
        redirect_to root_path
      else
        redirect_to root
      end

    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def index
    if params[:tag]
      @posts = Post.tagged_with(params[:tag]).order("created_at DESC").page(params[:page]).per(8)
    elsif params[:search]
      @posts = Post.search(params[:search]).order("created_at DESC").page(params[:page]).per(8)
    else
      @posts = Post.all.order("created_at DESC").page(params[:page]).per(8)
    end
  end

  def show
    if @category = Category.find_by!(permalink: params[:category])
      unless @post = @category.posts.find_by!(permalink: params[:permalink])
      end
    end
    @similar = Post.first(4)
    @comment = @post.comments.build
    @comments = @post.comments.all
  end

  def destroy
    if logged_in? && current_user.admin?
      @post = Post.find_by!(permalink: params[:id])
      @post.destroy
      redirect_to admin_path
    else
      redirect_to "/"
      flash[:error] = "Please Don't :("
    end
  end

  private
    def post_params
      params.require(:post).permit(:title, :markdown, :html, :meta, :permalink, :codepen, :source, :image, :tag_list, :category_id, :created_at)
    end
end
