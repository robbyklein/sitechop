class ResourcesController < ApplicationController
  def new
    if logged_in? && current_user.admin?
      @resource = Resource.new
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def create
    if logged_in? && current_user.admin?

      @resource = Resource.new(resource_params)

      if @resource.save
        redirect_to @resource.url
      else
        redirect_to root_url
      end

    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def edit
    if logged_in? && current_user.admin?
      @resource = Resource.find_by!(params[:id])
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def update
    if logged_in? && current_user.admin?

      @resource = Resource.find_by!(params[:id])

      if @resource.update(resource_params)
        redirect_to @resource
      else
        redirect_to root
      end
    else
      redirect_to "/"
      flash[:error] = "You don't belong there."
    end
  end

  def index
    @resources = Resource.all.order("created_at DESC").page(params[:page]).per(12)
  end

  def show
    @resource = Resource.find_by!(permalink: params[:permalink])
  end

  def destroy
    if logged_in? && current_user.admin?
      @resource = Resource.find_by!(params[:id])
      @resource.destroy
      redirect_to admin_path
    else
      redirect_to "/"
      flash[:error] = "Please Don't :("
    end
  end

  private
    def resource_params
      params.require(:resource).permit(:title, :description, :meta, :permalink, :file, :image)
    end
end
