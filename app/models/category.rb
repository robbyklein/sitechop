class Category < ActiveRecord::Base
  has_many :posts
  before_save :set_permalink


  # Category Image
  has_attached_file :image,
                  :url  => "/images/categories/:id/:basename.:extension",
                  :path => ":rails_root/public/images/categories/:id/:basename.:extension"

  do_not_validate_attachment_file_type :image

  def to_param
    self.permalink
  end

  def url
    "/#{self.permalink}"
  end

  private
  def set_permalink
    if self.permalink.present?
      self.permalink = self.permalink
    else
      self.permalink = self.title.parameterize
    end
  end
end
