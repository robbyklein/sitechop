class Post < ActiveRecord::Base
  belongs_to :category
  has_many :comments
  acts_as_taggable

  # Set everything on save
  before_save :markdown_to_html, :set_permalink, :set_meta

  # Source Code
  has_attached_file :source,
                  :url  => "/resources/source/:id/:basename.:extension",
                  :path => ":rails_root/public/resources/source/:id/:basename.:extension"

  do_not_validate_attachment_file_type :source

  # Post Image
  has_attached_file :image,
                  :url  => "/images/post_images/:id/:basename.:extension",
                  :path => ":rails_root/public/images/post_images/:id/:basename.:extension"

  do_not_validate_attachment_file_type :image
 
  before_post_process :skip_for_zip
  
  def self.search(query)
  # where(:title, query) -> This would return an exact match of the query
  where("title like ?", "%#{query}%") 
  end

  def skip_for_zip
     ! %w(application/zip application/x-zip).include?(source_content_type)
  end



  # Converts markdown
  def markdown_to_html
    # Graphs first paragraph
    paragraphs = self.markdown.split("\n")
    self.excerpt = paragraphs[0]
  
    # Removes first paragraph
    paragraphs.shift
    paragraphs = paragraphs.join("\n")
    
    # Processes markdown and sets html
    renderOptions = {filter_html: false}
    markdownOptions = {autolink: true, no_intra_emphasis: true, fenced_code_blocks: true}
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new(renderOptions), markdownOptions)
    self.html = markdown.render(paragraphs)
  end

  def to_param
    self.permalink
  end


  def url
    "/#{self.category.permalink}/#{self.permalink}"
  end

  # Sets meta description
  def set_meta
    self.meta = self.markdown[0..156] + "..."
  end

  # Sets codepen url
  def codepen_url
    "http://codepen.io/robbyk/pen/#{self.codepen}"
  end

  private
  def set_permalink
    self.permalink = self.title.parameterize
  end
end