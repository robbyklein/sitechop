class Resource < ActiveRecord::Base
  before_save :set_permalink

  # Source Code
  has_attached_file :file,
                  :url  => "/resources/solo/:id/:basename.:extension",
                  :path => ":rails_root/public/resources/solo/:id/:basename.:extension"

  do_not_validate_attachment_file_type :file

  # Source Code
  has_attached_file :image,
                  :url  => "/resources/solo/:id/:basename.:extension",
                  :path => ":rails_root/public/resources/solo/:id/:basename.:extension"

  do_not_validate_attachment_file_type :image

  def url
    "/resources/#{self.permalink}"
  end

  def set_permalink
    self.permalink = self.title.parameterize
  end
end
